variable "LINODE_TOKEN" {}
variable "PUBLIC_SSH_KEY" {}
variable "ROOT_PASSWORD" {}

provider "linode" {
	token = var.LINODE_TOKEN
}

resource "linode_instance" "keybasebots" {
	image = "linode/ubuntu18.04"
	label = "keybasebots"
	group = "terraform"
	region = "ca-central"
	type = "g6-nanode-1"
	authorized_keys = [ var.PUBLIC_SSH_KEY ]
	root_pass = var.ROOT_PASSWORD
}
