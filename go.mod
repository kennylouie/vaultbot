module gitlab.com/kennylouie/vaultbot

go 1.15

require (
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/keybase/go-keybase-chat-bot v0.0.0-20200505163032-5cacf52379da
	github.com/sirupsen/logrus v1.7.0
	gitlab.com/3headlabs/phrases v0.0.0-20201225071707-13c380677961
)
