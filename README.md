# VaultBot

## Introduction

VaultBot is a chatbot that helps store key-value secrets into a store.

Current implementation is to store secrets into keybase's kvstore.

## Usage

Dependencies:
+ Requires access to our chat server/router packages (currently private repo)

### From dockerfile

Required build args:
- gitlab_username - the gitlab username (when making token)
- gitlab_token - the gitlab token

```
docker build --build-arg gitlab_
username=<GITLAB_USER> --build-arg gitlab_token=<GITLAB_TOKEN> -t <IMAGE_NAME> .

```

Required environment variables when running dockerfile:
- `BOT_NAME` - the name of the bot that will respond when writing messages e.g. `vaultbot`
- `USER` - the keybase username to create the namespace for secrets (can be yourself)
- `APP_NAME` - the command that users will write to activate the bot e.g. ```!vault [route]```

```
docker run --rm \
    -e KEYBASE_USERNAME=<KEYBASE_USERNAME> \
    -e KEYBASE_PAPERKEY=<KEYBASE_PAPERKEY> \
    -e KEYBASE_SERVICE="1" \
    -e BOT_NAME=<BOT_NAME> \
    -e USER=<USER> \
    -e APP_NAME=<APP_NAME> \
    <IMAGE_NAME>
```
