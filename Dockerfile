FROM golang:1.13.10-buster as BUILDER

WORKDIR /vaultbot

ADD . .

# gitlab keys
ARG gitlab_username
ENV GITLAB_USERNAME=$gitlab_username
ARG gitlab_token
ENV GITLAB_TOKEN=$gitlab_token

# gitlab private deps
ENV GOPRIVATE="gitlab.com/3headlabs"

# set global git config
RUN git config \
    --global \
    url."https://${GITLAB_USERNAME}:${GITLAB_TOKEN}@gitlab.com".insteadOf \
    "https://gitlab.com"

RUN go build -o vaultbot ./cmd/vaultbot.go

FROM keybaseio/client

ENV KEYBASE_SERVICE=1

COPY --from=BUILDER /vaultbot/vaultbot /bin/.

CMD ["vaultbot"]
