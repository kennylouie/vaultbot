package main

import (
	"fmt"
	"os"

	"gitlab.com/kennylouie/vaultbot/pkg/app"
	"gitlab.com/kennylouie/vaultbot/pkg/conf"
	kbstore "gitlab.com/kennylouie/vaultbot/pkg/store/keybase"
	"gitlab.com/kennylouie/vaultbot/pkg/vault"

	"gitlab.com/3headlabs/phrases"
	"gitlab.com/3headlabs/phrases/keybase"
	"gitlab.com/3headlabs/phrases/router"
	"gitlab.com/3headlabs/phrases/server"

	log "github.com/sirupsen/logrus"
)

var (
	botName string
	user    string
	appName string
)

func init() {
	// logger setup
	log.SetFormatter(&log.JSONFormatter{})
	log.SetOutput(os.Stdout)
	log.SetLevel(log.DebugLevel)

	// set environment variables
	// all should be set from here
	config, err := conf.New()
	if err != nil {
		log.Fatal(err)
	}

	botName = config.GetBotname()
	user = config.GetUser()
	appName = config.GetAppName()
}

func main() {
	// logger
	logger := log.New()

	// chat message producer
	chat, err := keybase.New()
	if err != nil {
		logger.Fatal(err)
	}
	// router
	routerInstance := router.New(chat)
	// server
	serverInstance := server.New(chat, chat, routerInstance)

	// store for services
	store, err := kbstore.New(appName)
	if err != nil {
		logger.Fatal(fmt.Sprintf("could not create keybase api: %s", err))
	}
	// vault service
	vaultInstance := vault.New(user, appName, botName, store, logger)

	// chat bot
	bot := app.New(routerInstance, serverInstance)
	bot.LoadServices(vaultInstance).LoadMiddleware(
		func(msg phrases.Phrase) error {
			logger.WithFields(log.Fields{
				"sender":  msg.GetSender(),
				"chatMsg": msg.GetMessage(),
			}).Info("new message event")
			return nil
		},
	)

	logger.Info("Starting vaultbot service...")
	bot.Serve()
}
