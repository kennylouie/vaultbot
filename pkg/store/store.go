package store

type Store interface {
	List(string) (string, error)
	Get(string, string) (string, error)
	Put(string, string, string) error
	Delete(string, string) error
}
