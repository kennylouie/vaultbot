package keybase

import (
	"github.com/keybase/go-keybase-chat-bot/kbchat"
)

type keybase struct {
	client    *kbchat.API
	namespace string
}

const (
	kbLoc = "keybase"
)

func New(namespace string) (*keybase, error) {
	kbc, err := kbchat.Start(kbchat.RunOptions{KeybaseLocation: kbLoc})
	if err != nil {
		return nil, err
	}

	return &keybase{
		client:    kbc,
		namespace: namespace,
	}, nil
}

func (k keybase) Put(team string, secretkey, secretValue string) error {
	_, err := k.client.PutEntryWithRevision(&team, k.namespace, secretkey, secretValue, 0)
	if err != nil {
		return err
	}

	return nil
}

func (k keybase) Get(team, secretKey string) (secretValue string, err error) {
	res, err := k.client.GetEntry(&team, k.namespace, secretKey)
	if err != nil {
		return secretValue, err
	}

	if res.EntryValue != nil {
		secretValue = *(res.EntryValue)
	}

	return secretValue, err
}

func (k keybase) Delete(team, secretKey string) error {
	_, err := k.client.DeleteEntryWithRevision(&team, k.namespace, secretKey, 0)
	if err != nil {
		return err
	}

	return nil
}

func (k keybase) List(team string) (secrets string, err error) {
	res, err := k.client.ListEntryKeys(&team, k.namespace)
	if err != nil {
		return secrets, err
	}

	for _, v := range res.EntryKeys {
		secrets = secrets + v.EntryKey + "\n"
	}

	return secrets, nil
}
