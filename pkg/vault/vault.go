package vault

import (
	"fmt"
	"strings"

	"gitlab.com/kennylouie/vaultbot/pkg/app"
	"gitlab.com/kennylouie/vaultbot/pkg/eventstore"
	"gitlab.com/kennylouie/vaultbot/pkg/store"

	"github.com/sirupsen/logrus"
	"gitlab.com/3headlabs/phrases/router"
)

type vault struct {
	user    string
	botName string
	appName string
	store   store.Store
	logger  *logrus.Logger
}

func New(user, appName, botName string, s store.Store, logger *logrus.Logger) *vault {
	return &vault{
		user:    user,
		botName: botName,
		appName: appName,
		store:   s,
		logger:  logger,
	}
}

func (v *vault) GetRoutes() []app.Route {
	return []app.Route{
		app.Route{
			Name:    "LIST",
			Path:    fmt.Sprintf("%s list", v.appName),
			Handler: v.secretsListHandler,
		},
		app.Route{
			Name:    "PUT",
			Path:    fmt.Sprintf("%s put", v.appName),
			Handler: v.secretsPutHandler,
		},
		app.Route{
			Name:    "GET",
			Path:    fmt.Sprintf("%s get", v.appName),
			Handler: v.secretsGetHandler,
		},
		app.Route{
			Name:    "DELETE",
			Path:    fmt.Sprintf("%s delete", v.appName),
			Handler: v.secretsDeleteHandler,
		},
	}
}

// New route handler to echo back send message to the route
func (v vault) secretsListHandler(r *router.Request, w *router.Response) {
	p := r.GetPhrase()
	sender := p.GetSender()

	secrets, err := v.store.List(constructTeam(v.user, sender))
	if err != nil {
		v.logger.Warn(fmt.Sprintf("could not fetch keybase secrets: %s", err))
		return
	}

	if len(secrets) == 0 {
		w.Write(v.respond("none"), sender)
		return
	}

	w.Write(v.respond(fmt.Sprintf("\n%s", secrets)), sender)
}

func (v vault) secretsPutHandler(r *router.Request, w *router.Response) {
	// secret key and value
	p := r.GetPhrase()
	sender := p.GetSender()

	msg := strings.Split(p.GetMessage(), " ")

	// get current list
	// get value from store
	secretValue, err := v.store.Get(constructTeam(v.user, sender), msg[2])
	if err != nil {
		errMsg := fmt.Sprintf("could not get %s from keybase secrets: %s", msg[2], err)
		v.logger.Warn(errMsg)
		w.Write(v.respond(errMsg), sender)
		return
	}

	// deserialize from string
	es, err := eventstore.New(secretValue, nil)
	if err != nil {
		errMsg := fmt.Sprintf("could not deserialize %s from keybase secrets: %s", secretValue, err)
		v.logger.Warn(errMsg)
		w.Write(v.respond(errMsg), sender)
		return
	}
	es.AddEvent(msg[3])
	serialized, err := es.GetBase64StringEvents()
	if err != nil {
		errMsg := fmt.Sprintf("could not serialize %s with secret value of %s: %s", secretValue, msg[2], err)
		v.logger.Warn(errMsg)
		w.Write(v.respond(errMsg), sender)
		return
	}

	err = v.store.Put(constructTeam(v.user, sender), msg[2], serialized)
	if err != nil {
		errMsg := fmt.Sprintf("could not put %s into keybase secrets: %s", msg[2], err)
		v.logger.Warn(errMsg)
		w.Write(v.respond(errMsg), sender)
		return
	}

	w.Write(v.respond(fmt.Sprintf("added - %s", msg[2])), sender)
}

func (v vault) secretsGetHandler(r *router.Request, w *router.Response) {
	// get secret key
	p := r.GetPhrase()
	sender := p.GetSender()

	msg := strings.Split(p.GetMessage(), " ")

	// get value from store
	secretValue, err := v.store.Get(constructTeam(v.user, sender), msg[2])
	if err != nil {
		errMsg := fmt.Sprintf("could not get %s from keybase secrets: %s", msg[2], err)
		v.logger.Warn(errMsg)
		w.Write(v.respond(errMsg), sender)
		return
	}

	// deserialize from string
	es, err := eventstore.New(secretValue, nil)
	if err != nil {
		errMsg := fmt.Sprintf("could not deserialize %s from keybase secrets: %s", secretValue, err)
		v.logger.Warn(errMsg)
		w.Write(v.respond(errMsg), sender)
		return
	}
	events := es.GetEventStore()
	if len(events) == 0 {
		w.Write(v.respond(""), sender)
		return
	}

	// only get latest
	latest := events[len(events)-1]

	w.Write(v.respond(fmt.Sprintf("last update: %s", latest.Timestamp)), sender)
	w.Write(latest.Message, sender)
}

func (v vault) secretsDeleteHandler(r *router.Request, w *router.Response) {
	p := r.GetPhrase()
	sender := p.GetSender()

	msg := strings.Split(p.GetMessage(), " ")

	err := v.store.Delete(constructTeam(v.user, sender), msg[2])
	if err != nil {
		errMsg := fmt.Sprintf("could not delete %s in keybase kvstore: %s", msg[2], err)
		v.logger.Warn(errMsg)
		w.Write(v.respond(errMsg), sender)
		return
	}

	w.Write(v.respond(fmt.Sprintf("deleted - %s", msg[2])), sender)
}

func constructTeam(p1, p2 string) string {
	return fmt.Sprintf("%s,%s", p1, p2)
}

func (v vault) respond(message string) string {
	return fmt.Sprintf("%s: %s\n", v.botName, message)
}
