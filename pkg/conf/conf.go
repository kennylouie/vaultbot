package conf

import (
	"github.com/kelseyhightower/envconfig"
)

type conf struct {
	BOT_NAME string `required:"true"`
	USER     string `required:"true"`
	APP_NAME string `required:"true"`
}

func New() (conf, error) {
	config := conf{}

	err := envconfig.Process("", &config)
	if err != nil {
		return config, err
	}

	return config, nil
}

func (c conf) GetBotname() string {
	return c.BOT_NAME
}

func (c conf) GetUser() string {
	return c.USER
}

func (c conf) GetAppName() string {
	return c.APP_NAME
}
