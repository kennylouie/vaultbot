package eventstore

import (
	"encoding/base64"
	"encoding/json"
	"testing"
)

type timeProviderMock struct{}

func (t timeProviderMock) Now() string {
	return "2009-11-10t23:00:00z"
}

// unit tests for instantiating new eventstore
type newEventStoreTest struct {
	title          string
	input          string
	expectedEvents []Event
}

func TestNewEventStore(t *testing.T) {
	tests := []newEventStoreTest{
		newEventStoreTest{
			title:          "should create new empty store with empty string param",
			input:          "",
			expectedEvents: []Event{},
		},
		newEventStoreTest{
			title: "should create eventStore with based on base64 string param",
			input: "W3sidGltZXN0YW1wIjoiMjAwOS0xMS0xMFQyMzowMDowMFoiLCJtZXNzYWdlIjoidGVzdCJ9XQ==",
			expectedEvents: []Event{
				Event{
					Timestamp: "2009-11-10t23:00:00z",
					Message:   "test",
				},
			},
		},
	}

	for _, test := range tests {
		t.Run(test.title, func(t *testing.T) {
			testNewEventStore(t, test)
		})
	}
}

func testNewEventStore(t *testing.T, test newEventStoreTest) {
	es, err := New(test.input, timeProviderMock{})
	if err != nil {
		t.Fatalf("Could not create new eventstore: %v\n", err)
	}

	if len(test.expectedEvents) != len(es.events) {
		t.Errorf("Expected %v but got %v\n", len(test.expectedEvents), len(es.GetEventStore()))
	}

	if len(test.input) > 0 {
		bytes, err := base64.StdEncoding.DecodeString(test.input)
		if err != nil {
			t.Fatalf("Could not convert input string to bytes: %v\n", err)

		}

		expectedEvents := []Event{}

		err = json.Unmarshal(bytes, &expectedEvents)
		if err != nil {
			t.Fatalf("Could not deserialize bytes to events array: %v\n", err)
		}

		expectedMessage := expectedEvents[0].Message
		actualMessage := es.events[0].Message
		if expectedMessage != actualMessage {
			t.Errorf("Expected %v but got %v\n", expectedMessage, actualMessage)
		}
	}
}

// unit tests for adding new event to eventstore
type addEventStoreTest struct {
	title   string
	message string
}

func TestAddEventStore(t *testing.T) {
	tests := []addEventStoreTest{
		addEventStoreTest{
			title:   "should add a new event to an existing eventStore",
			message: "test",
		},
	}

	for _, test := range tests {
		t.Run(test.title, func(t *testing.T) {
			testAddEventStore(t, test)
		})
	}
}

func testAddEventStore(t *testing.T, test addEventStoreTest) {
	eventStore, err := New("", timeProviderMock{})
	if err != nil {
		t.Fatalf("Could not create new eventstore: %v\n", err)
	}

	eventStore.AddEvent(test.message)

	actual := eventStore.events[len(eventStore.events)-1].Message

	if test.message != actual {
		t.Errorf("Expected %v but got %v\n", test.message, actual)
	}
}

// unit tests for serializing the eventStore to a base64 string
type getBase64StringTest struct {
	title          string
	expectedString string
	eventStore     eventStore
}

func TestGetBase64String(t *testing.T) {
	tests := []getBase64StringTest{
		getBase64StringTest{
			title:          "should get the base64 of an eventStore with data",
			expectedString: "W3sidGltZXN0YW1wIjoiMjAwOS0xMS0xMFQyMzowMDowMFoiLCJtZXNzYWdlIjoidGVzdCJ9XQ==",
			eventStore: eventStore{
				events: []Event{
					Event{
						Timestamp: "2009-11-10T23:00:00Z",
						Message:   "test",
					},
				},
			},
		},
		getBase64StringTest{
			title:          "should get the base64 of an empty eventStore",
			expectedString: "W10=",
			eventStore: eventStore{
				events: []Event{},
			},
		},
	}

	for _, test := range tests {
		t.Run(test.title, func(t *testing.T) {
			testGetBase64String(t, test)
		})
	}
}

func testGetBase64String(t *testing.T, test getBase64StringTest) {
	actualString, err := test.eventStore.GetBase64StringEvents()
	if err != nil {
		t.Fatalf("Could not get base64 string of the eventStore: %v\n", err)
	}

	if actualString != test.expectedString {
		t.Errorf("Expected %v but got %v\n", test.expectedString, actualString)
	}
}
