package eventstore

import (
	"gitlab.com/kennylouie/vaultbot/pkg/eventstore/serializer"
)

// Takes a base64 string and decodes to bytes
func toBytes(s string) ([]byte, error) {
	serializer := serializer.New()

	b, err := serializer.Decode(s)
	if err != nil {
		return []byte{}, err
	}

	return b, nil
}

// takes bytes and converts to base64 encoded string
func toString(b []byte) string {
	serializer := serializer.New()
	return serializer.Encode(b)
}
