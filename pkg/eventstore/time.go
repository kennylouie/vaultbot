package eventstore

import (
	"time"
)

type TimeProvider interface {
	Now() string
}

type timeWrapper struct{}

func (t timeWrapper) Now() string {
	return time.Now().UTC().Format(time.RFC3339Nano)
}
