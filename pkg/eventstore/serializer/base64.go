package serializer

import (
	"encoding/base64"
)

type base64Serializer struct{}

func New() *base64Serializer {
	return &base64Serializer{}
}

func (b base64Serializer) Encode(bytes []byte) string {
	return base64.StdEncoding.EncodeToString(bytes)
}

func (b base64Serializer) Decode(s string) ([]byte, error) {
	return base64.StdEncoding.DecodeString(s)
}
