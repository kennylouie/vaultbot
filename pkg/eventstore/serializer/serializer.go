package serializer

type Serializer interface {
	Encode(bytes []byte) string
	Decode(s string) ([]byte, error)
}
