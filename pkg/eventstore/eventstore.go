package eventstore

import (
	"encoding/json"
)

type eventStore struct {
	events       []Event
	timeProvider TimeProvider
}

type Event struct {
	Timestamp string `json:"timestamp"`
	Message   string `json:"message"`
}

// creates a new eventstore
// holds a current eventstore store
// or creates a new one
func New(eventStoreString string, timeProvider TimeProvider) (*eventStore, error) {
	if timeProvider == nil {
		timeProvider = timeWrapper{}
	}

	events := []Event{}

	if len(eventStoreString) > 0 {
		bytes, err := toBytes(eventStoreString)
		if err != nil {
			return nil, err
		}

		err = json.Unmarshal(bytes, &events)
		if err != nil {
			return nil, err
		}
	}

	return &eventStore{
		events:       events,
		timeProvider: timeProvider,
	}, nil
}

// returns the events as a base64 string
func (e eventStore) GetBase64StringEvents() (events string, err error) {
	bytes, err := json.Marshal(e.events)
	if err != nil {
		return events, err
	}

	return toString(bytes), nil
}

func (e eventStore) GetEventStore() []Event {
	return e.events
}

// adds a new event to the store
func (e *eventStore) AddEvent(message string) {
	newEvent := Event{
		Timestamp: e.timeProvider.Now(),
		Message:   message,
	}

	e.events = append(e.events, newEvent)
}
