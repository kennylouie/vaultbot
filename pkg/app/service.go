package app

type Service interface {
	GetRoutes() []Route
}
