package app

import (
	"log"

	"gitlab.com/3headlabs/phrases"
	"gitlab.com/3headlabs/phrases/router"
	"gitlab.com/3headlabs/phrases/server"
)

type app struct {
	router router.Router
	server server.Server
}

func New(r router.Router, s server.Server) *app {
	return &app{
		router: r,
		server: s,
	}
}

func (a *app) LoadServices(s Service) *app {
	routes := s.GetRoutes()

	for _, route := range routes {
		err := a.router.AddRoute(route.Path, route.Handler)
		if err != nil {
			log.Fatalf("Could not load %s route\n", route.Name)
		}
	}

	return a
}

func (a *app) LoadMiddleware(f func(msg phrases.Phrase) error) *app {
	a.server.AddMiddleware(f)
	return a
}

func (a app) Serve() {
	a.server.Serve()
}
