package app

import (
	"gitlab.com/3headlabs/phrases/router"
)

type Route struct {
	Name    string
	Path    string
	Handler router.HandlerFunc
}
